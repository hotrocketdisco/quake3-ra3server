# Builder image
FROM debian:stable-slim as builder
RUN \
    apt-get update && apt-get -y --no-install-recommends install \
        ca-certificates \
        curl \
        git \
        make \
        unzip \
        g++ \
        gcc-multilib \
        build-essential \
    && \
    mkdir -p /tmp/build && \
    export ARCH=x86 && \
    export PLATFORM=linux && \
    curl https://raw.githubusercontent.com/ioquake/ioq3/master/misc/linux/server_compile.sh -o /tmp/build/compile.sh && \
    curl https://files.ioquake3.org/quake3-latest-pk3s.zip -k -o /tmp/build/quake3-latest-pk3s.zip && \
    echo "y" | sh /tmp/build/compile.sh && \
    unzip -q /tmp/build/quake3-latest-pk3s.zip -d /tmp/build/ && \
    cp -r /tmp/build/quake3-latest-pk3s/* ~/ioquake3
COPY ./game /tmp/arena
RUN \
    unzip -q /tmp/arena/ra3176.zip -d ~/ioquake3 && \
    unzip -o -q /tmp/arena/ra3176asv.zip -d ~/ioquake3/arena && \
    rm -R /tmp/build && \
    rm -R /tmp/arena

 
# Rocket Arena Server image
FROM alpine:latest

# Configuration
ENV Q3_USER ioq3srv
ENV Q3_GAME_FOLDER /home/ioq3srv/ioquake3
ENV Q3_SERVER_PORT 27960
ENV RA3_INITIAL_MAP ra3map1
ENV RA3_SERVER_MODE 1
ENV RA3_SERVER_PURE 1
ENV RA3_SERVER_HOSTNAME "Rocket Arena 3 1.7 Server"
ENV RA3_SERVER_MODT "Welcome to Rocket Arena 3..."
ENV RA3_MAX_CLIENTS 16
ENV RA3_TIME_LIMIT 30
ENV RA3_ADMIN_PASSWORD 123456
ENV RA3_RCON_PASSWORD 123456
ENV RA3_RCON_ENABLED false
ENV RA3_PRIVATE_PASSWORD 123456
ENV RA3_PRIVATE_ENABLED false
ENV RA3_PRIVATE_SLOTS 0

# Copy game from builder
COPY --from=builder /root/ioquake3 ${Q3_GAME_FOLDER}

# Copy config template
COPY ./config/server.tpl /tmp

# Create pak folders and install game
RUN \
    mkdir /paks && \
    mkdir /paks/baseq3 && \
    mkdir /paks/arena && \
    ls -la ${Q3_GAME_FOLDER} && \
    ls -la ${Q3_GAME_FOLDER}/arena
    
# Entrypoint script
COPY ./entrypoint.sh /usr/local/bin/
RUN \
    chmod +x /usr/local/bin/entrypoint.sh && \
    ln -s /usr/local/bin/entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# Network
EXPOSE ${Q3_SERVER_PORT}/udp

# Set workdir to game folder
WORKDIR ${Q3_GAME_FOLDER}

# Create user
RUN adduser ${Q3_USER} -D
USER ${Q3_USER}

# Default command
CMD ["--run"]