// Set your server's name
set sv_hostname "{RA3_SERVER_HOSTNAME}"

// This message will scroll on the bottom of the arena selection menu
set g_motd "{RA3_SERVER_MODT}"

// The name of the arena config file. 
set arenacfg "arena.cfg"

// Set the max number of clients to connect
set sv_maxclients {RA3_MAX_CLIENTS}

// Set the time to cycle maps, ra3 does not use fraglimit, only timelimit
set timelimit {RA3_TIME_LIMIT}

// Uncomment this var to reserve spots for people with the private passowrd
{RA3_PRIVATE_ENABLED} set sv_privateClients {RA3_PRIVATE_SLOTS}

// Set the private password to use the private spots with
set sv_privatePassword "{RA3_PRIVATE_PASSWORD}"

// Uncomment the line below to allow in-game admin with the password given below
set g_adminpass "{RA3_ADMIN_PASSWORD}"

// Uncomment the line below to set the remote console password
{RA3_RCON_ENABLED}set rconPassword "{RA3_RCON_PASSWORD}"

// Memory allocated for server
set com_hunkmegs "64"

// Set sv_floodprotect to 0
set sv_floodprotect "0"

// Chat flood protection
set g_chatFlood "5:5:2"

// Minimum of seconds between allowed callvote command
set g_voteInterval 30

// Votepercent for callvote commands.
set g_votePercent "60"

// Set server fps 
set sv_fps "30"

// Use the following for location:
// Region 0: No Region Specified (any Region)
// Region 1: Southeast US
// Region 2: Western US
// Region 3: Midwest US
// Region 4: Northwest US, West Canada
// Region 5: Northeast US, East Canada
// Region 6: United Kingdom
// Region 7: Continental Europe
// Region 8: Central Asia, Middle East
// Region 9: Southeast Asia, Pacific
// Region 10: Africa
// Region 11: Australia / NZ / Pacific
// Region 12: Central, South America
sets location 0

// Set the initial map
map {RA3_INITIAL_MAP}