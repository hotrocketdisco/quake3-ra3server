# Quake 3 Rocket Arena Server

This docker container runs a Quake 3 Linux server with the Rocket Arena 3 (arena) mod.

## About this container

The container is an ioquake3 based Rocket Arena 3 server based on an Alpine Linux. See (http://theblackzone.net/howto-install-quake-3-arena-on-xubuntu-1404) why Quake otherwise would not run on a default Alpine Linux.

## Quake 3 *.pk3 files

This repository does not include the `pak0.pk3` file which is required by the server as it cannot not be distributed. To add this file to your container, map a host path containing this file into the `/paks/baseq3` folder.

## Build argumnets

* IMAGE_TAG
* IMAGE_BASE
* Q3\_PAK\_FILES <span style="color:blue">*./paks/baseq3*</span>
* Q3\_GAME\_FILES <span style="color:blue">*./game*</span>
* Q3\_GAME\_FOLDER <span style="color:blue">*/quake3*</span>
* RA3\_PAK\_FILES <span style="color:blue">*./paks/arena*</span>
* RA3\_CONFIG\_FILES <span style="color:blue">*./config*</span>
* ENTRYPOINT_SCRIPT <span style="color:blue">*./entrypoint.sh*</span>

## Supported environment variables

You can run the container image with custom Quake 3 / Rocket Arena 3 settings by providing the respective environment variables

* Q3\_GAME\_FOLDER <span style="color:blue">*/quake3*</span>
* Q3\_SERVER\_PORT <span style="color:blue">*27960*</span>
* RA3\_INITIAL\_MAP <span style="color:blue">*ra3map1*</span>
* RA3\_SERVER\_MODE <span style="color:blue">*1*</span>
* RA3\_SERVER\_PURE <span style="color:blue">*1*</span>
* RA3\_SERVER\_HOSTNAME <span style="color:blue">*Rocket Arena 3 1.7 Server*</span>
* RA3\_SERVER\_MODT <span style="color:blue">*Welcome to Rocket Arena 3...*</span>
* RA3\_MAX\_CLIENTS <span style="color:blue">*16*</span>
* RA3\_TIME\_LIMIT <span style="color:blue">*30*</span>
* RA3\_ADMIN\_PASSWORD <span style="color:blue">*123456*</span>
* RA3\_RCON\_PASSWORD <span style="color:blue">*123456*</span>
* RA3\_RCON\_ENABLED <span style="color:blue">*false*</span>
* RA3\_PRIVATE\_PASSWORD <span style="color:blue">*123456*</span>
* RA3\_PRIVATE\_ENABLED <span style="color:blue">*false*</span>
* RA3\_PRIVATE\_SLOTS <span style="color:blue">*0*</span>

## How to build your container?

Build a default Rocket Arena container with your own tag:

    docker build -t {YOUR_IMAGE_TAG} .
    
The server requires at least the `pak0.pk3` file which must not be distributed. This file can be later mounted into your container or pre-packed into the image. To add your own `*.pk3` files, copy your files into the default `./paks` folder or use the following build command to specify a path:

    docker build -t {YOUR_IMAGE_TAG} --build-arg Q3_PAK_FILES={CUSTOM_PATH} .
    
## How to run the server?

Run a default Rocket Arena server at port 27960:

    docker run -it --rm --name {YOUR_IMAGE_NAME} -p 27960:27960/udp {YOUR_IMAGE_TAG}
    
To change your server settings on running the container simply provide environment variables like in the following example or use an `.env file`.
    
    docker run -i -t --rm --name {YOUR_IMAGE_NAME} -p 27960:27960/udp -e RA3_SERVER_HOSTNAME='Hot Rocket Disco' {YOUR_IMAGE_TAG}
    
You can also directly start a server by passing the respective server parameters to `q3ded`. In this case you simply use the image as a `q3ded` executable.

    docker run -i -t --rm --name {YOUR_IMAGE_NAME} -p 27960:27960/udp {YOUR_IMAGE_TAG} {ARG1} {ARG2} {ARG3} ...