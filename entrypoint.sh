#!/bin/sh

echo "################################"
echo "#                              #"
echo "#  Quake Rocketarena 3 Server  #"
echo "#                              #"
echo "################################"

case $1 in

--run)
    cp ${Q3_GAME_FOLDER}/arena/server.tpl ${Q3_GAME_FOLDER}/arena/server.cfg
    
    ln -sf /paks/baseq3/*.pak ${Q3_GAME_FOLDER}/baseq3/
    ln -sf /paks/arena/*.pak ${Q3_GAME_FOLDER}/arena/

    if [ "$RA3_PRIVATE_ENABLED" = true ]
        then
            sed -i "s/{RA3_PRIVATE_ENABLED}//g" ${Q3_GAME_FOLDER}/arena/server.cfg
        else
            sed -i "s/{RA3_PRIVATE_ENABLED}/\/\//g" ${Q3_GAME_FOLDER}/arena/server.cfg
    fi
    if [ "$RA3_RCON_ENABLED" = true ]
        then
            sed -i "s/{RA3_RCON_ENABLED}//g" ${Q3_GAME_FOLDER}/arena/server.cfg
        else
            sed -i "s/{RA3_RCON_ENABLED}/\/\//g" ${Q3_GAME_FOLDER}/arena/server.cfg
    fi
    sed -i "s/{RA3_INITIAL_MAP}/${RA3_INITIAL_MAP}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_SERVER_HOSTNAME}/${RA3_SERVER_HOSTNAME}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_SERVER_MODT}/${RA3_SERVER_MODT}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_MAX_CLIENTS}/${RA3_MAX_CLIENTS}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_TIME_LIMIT}/${RA3_TIME_LIMIT}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_ADMIN_PASSWORD}/${RA3_ADMIN_PASSWORD}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_RCON_PASSWORD}/${RA3_RCON_PASSWORD}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_PRIVATE_PASSWORD}/${RA3_PRIVATE_PASSWORD}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    sed -i "s/{RA3_PRIVATE_SLOTS}/${RA3_PRIVATE_SLOTS}/g" ${Q3_GAME_FOLDER}/arena/server.cfg
    
    echo "...starting default server (ioq3ded)"
    CMD="${Q3_GAME_FOLDER}/ioq3ded.x86 +set fs_game arena +set vm_game 0 +set sv_pure ${RA3_SERVER_PURE} +set bot_enable 0 +set dedicated ${RA3_SERVER_MODE} +set net_port ${Q3_SERVER_PORT} +exec server.cfg"
    ;;

--help)
    echo "This docker image can be used as executable."
    CMD="echo Use 'docker run {IMAGE} ARG1 ARG2 ARG3...' to pass your parameters to the Quake3 server (ioq3ded)!"
    ;;

*)
    echo "...starting server (ioq3ded) with the following arguments"
    echo ioq3ded.x86 "$@"
    CMD="${Q3_GAME_FOLDER}/ioq3ded.x86 $@"
    ;;

esac

exec $CMD